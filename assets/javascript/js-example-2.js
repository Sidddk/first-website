// break

$(function() { setTimeout(function() { $("#hideDiv").fadeOut(200); }, 5000)
    })

// break


//Loading
const textureLoader = new THREE.TextureLoader()

const normalTexture = textureLoader.load('./assets/images/NormalMap.png')



// Canvas
const canvas = document.querySelector('canvas.webgl')


// Scene
const scene = new THREE.Scene()


// Objects
const geometry = new THREE.SphereBufferGeometry(.5, 64, 64)


// Materials

const material = new THREE.MeshStandardMaterial()
material.metalness = 0.7
material.roughness = 0.2

material.normalMap = normalTexture;

material.color = new THREE.Color(0x292929)


// Mesh
const sphere = new THREE.Mesh(geometry,material)
scene.add(sphere)


// Lights

// Light 1
const pointLight = new THREE.PointLight(0xffffff, 0.1)
pointLight.position.x = 2
pointLight.position.y = 3
pointLight.position.z = 4
scene.add(pointLight)

// Light 2
const pointLight2 = new THREE.PointLight(0x00e1ff, 2)
// const pointLight2 = new THREE.PointLight(0xff0000, 2)
pointLight2.position.set(-1.86,1,-1.65)
pointLight2.intensity = 10

scene.add(pointLight2)


// Light 3
const pointLight3 = new THREE.PointLight(0xb02900, 2)
// const pointLight3 = new THREE.PointLight(0x00e1ff, 2)
pointLight3.position.set(2.13,-3,-1.98)
pointLight3.intensity = 6.8

scene.add(pointLight3)


// // Light 4 Ambient Light
// const colorAmb = 0xffc4e3;
// const intensityAmb = 1.4;
// const ambientLight = new THREE.AmbientLight(colorAmb, intensityAmb);
// // soft white light
// scene.add(ambientLight);



/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0
camera.position.y = 0
camera.position.z = 2
scene.add(camera)

// Controls
// const controls = new OrbitControls(camera, canvas)
// controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    alpha: true
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */


document.addEventListener('mousemove', onDocumentMouseMove)

let mouseX = 0;
let mouseY = 0;

let targetX = 0;
let targetY = 0;

const windowX = window.innerWidth / 2;
const windowY = window.innerHeight / 2;

function onDocumentMouseMove(event) {
    mouseX = (event.clientX - windowX)
    mouseY = (event.clientY - windowY)
}

// Sphere goes up when scrolling down
const updateSphere = (event) => {
    sphere.position.y = window.scrollY * 0.001
}
window.addEventListener('scroll', updateSphere);


// Sphere comes closer (looks bigger) when scrolling down
const updateSphere2 = (event) => {
    sphere.position.z = window.scrollY * 0.002
}
window.addEventListener('scroll', updateSphere2);



const clock = new THREE.Clock()

const tick = () =>
{

    targetX = mouseX * 0.001
    targetY = mouseY * 0.001


    const elapsedTime = clock.getElapsedTime()

    // Update objects
    sphere.rotation.y = .5 * elapsedTime

    sphere.rotation.x += .07 * (targetY - sphere.rotation.x)
    sphere.rotation.y += .6 * (targetX - sphere.rotation.y)
    sphere.position.z += -.07 * (targetY - sphere.rotation.x)

    // Update Orbital Controls
    // controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()



